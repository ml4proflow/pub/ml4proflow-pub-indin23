from setuptools import setup, find_namespace_packages

with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()

setup(
    name="ml4proflow-pub-indin23",
    version="0.0.1",
    author="Christian Klarhorst",
    author_email="cklarhor@techfak.uni-bielefeld.de",
    description="INDIN23 files",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.ub.uni-bielefeld.de/-/ide/project/ml4proflow/pub/ml4proflow-pub-indin23",
    project_urls={
        "Main framework": "https://gitlab.ub.uni-bielefeld.de/ml4proflow/ml4proflow",
    },
    classifiers=[
        "Development Status :: 4 - Beta",
        "Intended Audience :: Developers",
        "Programming Language :: Python :: 3",
        "Topic :: Scientific/Engineering :: Artificial Intelligence",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],

    #package_dir={"": "src"},
    package_dir={"": "src"},
    packages=find_namespace_packages(where="src"),
    #namespace_packages=['ml4proflow_mods'],
    #install_requires=["ml4proflow"], currently this pkg doesn't need ml4proflow
    extras_require={
        "tests": ["pytest", 
          "pytest-html",
          "pytest-cov",
          "flake8",
          "mypy"],
        "docs": ["sphinx", "sphinx-rtd-theme", "recommonmark"],
    },
    python_requires=">=3.6", # todo
)
